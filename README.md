# Summarized Network Histories #

## Introduction ##

With the advent of Software Defined Networking, programmable networks are easier than ever.
Such programmable networks allow a wide variety of network configurations and policies.
As the complexity of such networks increases, the complexity of maintaining them also increases.

This document discusses the importance and methods of maintaining the network state using condensed histories of network events.

## Tools ##

### OFRewind ###

[OFRewind](http://static.usenix.org/event/atc11/tech/final_files/Wundsam.pdf) is based on the split forwarding architecture, which is well implemented in projects like [OpenFlow](http://en.wikipedia.org/wiki/OpenFlow).
It has elements like switches for packet forwarding and the control plane is programmable.
This system aims at building a scalable recording system which will be able to establish a temporally view of the events happening in the network (as received by the controller) and must be able to replay the recorded events to reestablish the network state.

### NetSight ###

[NetSight](https://www.usenix.org/conference/nsdi14/technical-sessions/presentation/handigol) is used to generalize network diagnosis tools by using the notion of a __packet history__.
A packet history is the route a packet takes through a network plus the switch state and header modifications it encounters at each hop.
Such a packet history can be very useful in network diagnosis as it makes it easy to find exactly why, how or where did the network fail.

NetSight enables building several applications on top of itself like __ndb__ (interactive network debugger), __netwatch__ (live invariant monitor), __netshark__ (network monitoring tool similar to wireshark), __nprof__ (hierarchical network profiler), and many more.

## Conclusion ##

This report studies these tools and presents the ways to efficiently store and process network histories.
We conclude that while standard techniques like data compression using methods like gzip compression (See reference [[6]](http://www.hpl.hp.com/techreports/2006/HPL-2006-156.pdf)) or optimised file system (See reference [[7]](https://www.usenix.org/legacy/event/usenix07/tech/desnoyers/desnoyers.pdf)) seem to work, they do not aim at actually reducing the amount of packet/event history which has to processed by any application layer service.

As there is no limit on the types of network setup and configurations and each of these may require a different mechanism and rule for storing network histories, it is a good idea to use an API such as NetSight for accomplishing this task.


## Contact ##

* [Abdul Aziz](mailto:cs12b1001@iith.ac.in)
* [Agam Agarwal](mailto:cs12b1003@iith.ac.in)
* [Richik Jaiswal](mailto:cs12b1032@iith.ac.in)
* [Sanyam Kapoor](mailto:cs12b1043@iith.ac.in)
